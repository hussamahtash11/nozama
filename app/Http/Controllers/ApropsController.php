<?php

namespace App\Http\Controllers;

use App\Models\Aprops;
use Illuminate\Http\Request;

class ApropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
         return view('aprops');
     }

}
