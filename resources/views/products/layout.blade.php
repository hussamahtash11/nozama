<!DOCTYPE html>
<html>
<head>
    <title>Nozama</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    
<div class="container">
@extends('layouts.app')
    @yield('content')
</div>

</body>
</html>