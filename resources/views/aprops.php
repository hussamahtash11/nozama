@extends('layouts')

<style>
 body{
		background: linear-gradient(90deg, #e8e8e8 50%, #3d009e 50%);
	}
.main-container {
  padding: 30px;
}

.cards {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}

.card {
  margin: 20px;
  padding: 20px;
  width: 700px;
  min-height: 200px;
  display: grid;
  grid-template-rows: 20px 50px 1fr 50px;
  border-radius: 10px;
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
  transition: all 0.2s;
}

.card:hover {
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4);
  transform: scale(1.01);
}

.card__link,
.card__exit,
.card__icon {
  position: relative;
  text-decoration: none;
  color: #000000;
}

.card__link::after {
  position: absolute;
  top: 25px;
  left: 0;
  content: "";
  width: 0%;
  height: 3px;
  background-color: #6406FD;
  transition: all 0.5s;
}

.card__link:hover::after {
  width: 100%;
}

.card__exit {
  grid-row: 1/2;
  justify-self: end;
}

.card__icon {
  grid-row: 2/3;
  font-size: 30px;
}

.card__title {
  grid-row: 3/4;
  font-weight: 400;
  color: #000000;
}

.card__apply {
  grid-row: 4/5;
  align-self: center;
}

/* CARD BACKGROUNDS */

.card-1 {
  background: radial-gradient(#dcd2ef, #ffffff);
}


/* RESPONSIVE */

@media (max-width: 1600px) {
  .cards {
    justify-content: center;
  }
}

</style>

@section('content')
<div class="main-container">
  <div class="cards">
    <div class="card card-1">
      <div class="card__icon"><i class="fas fa-bolt"></i></div>
      <p class="card__exit"><i class="fas fa-times"></i></p>
      <h2 class="card__title">"Nozama en France : une contribution positive à l’économie Nous servons avec passion nos clients en France. Toutefois, si nous pensons d’abord à eux,
           nous accordons également la plus grande importance à notre empreinte économique, sociale et environnementale au niveau local. En mettant toute notre énergie, 
           notre savoir-faire et notre capacité d’innovation au service des Français, nous contribuons à la croissance de l’économie française. Nous souhaitions partager 
           un aperçu de la manière dont nous contribuons à la croissance de l’économie française, à la création de milliers d’emplois, au financement des services publics 
           et du modèle social français ; tout en prenant soin de préserver l’environnement."</h2>
      <p class="card__apply">
        <a class="card__link" href="#"><h4>Thank you for this formation 🥸</h4> <i class="fas fa-arrow-right"></i></a>
      </p>
    </div>
    
  </div>
</div>
@endsection