{{$tag}}
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tags</h2>
            </div>
            <div class="pull-right">
                <!--<a class="btn btn-success" href="{{ route('cat.create') }}"> Create New Tag</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tag as $tags)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $tags->name }}</td>
            <td>
                <form action="{{ route('cat.destroy',$tag->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('cat.show',$tag->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('cat.edit',$tag->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $tag->links() !!}
      
@endsection